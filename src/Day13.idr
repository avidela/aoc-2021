module Day13

import Prolude
import Prolude.Parser
import Prolude.SortedMap
import Data.String.Parser
import Data.SortedSet
import Data.String
import System.File
import Debug.Trace

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

Coord : Type
Coord = (Nat, Nat)

data Instr = FoldX Nat | FoldY Nat

Show Instr where
  show (FoldX n) = "fold Vertical \{show n}"
  show (FoldY n) = "fold Horizontal \{show n}"

Input :  Type
Input = (List Coord, List Instr)

comma : Parser Char
comma = char ','

parseInput : String -> Either String Input
parseInput = map Builtin.fst . parse (MkPair <$> parseCoord <* spaces1 <*> parseInstr)
  where
    parseCoord : Parser (List Coord)
    parseCoord = (MkPair <$> (natural <* comma) <*> natural) `sepBy` spaces1

    parseInstr : Parser (List Instr)
    parseInstr = (token "fold" *> token "along" *>
                   (FoldY <$> (token "y=" *> natural)
                <|> FoldX <$> (token "x=" *> natural))) `sepBy` spaces1

Paper : Type
Paper = SortedSet Coord

printPaper : Paper -> String
printPaper x = let coords = SortedSet.toList x
                   xCoords = map (cast . Builtin.fst) coords
                   yCoords = map (cast . Builtin.snd) coords
                   cols = integerToNat $ PositiveInteger.maximum xCoords
                   lines = integerToNat $ PositiveInteger.maximum yCoords
                   allCoords = map (\x =>  (x, [0 .. lines])) [0 .. cols]
                   v = (map (\(x, line) =>
                           map (\y => if (elem (x, y) coords) then '#' else ' ')
                           $ line) allCoords)
                in unlines $ map (pack) (transpose v)

foldPaper : Instr -> Paper -> Paper
foldPaper (FoldX axis) paper =
  let (remove, keep) = partition (\(x, y) => x > axis) (SortedSet.toList paper)
      updated : List Coord = map (mapFst (\y =>
                       axis + axis `minus` y)) remove
      folded = fromList (keep ++ updated)
   in folded

foldPaper (FoldY axis) paper =
  let (remove, keep) = partition (\(x, y) => y > axis) (SortedSet.toList paper)
      updated = map (mapSnd (\x =>
                       axis + axis `minus` x)) remove
      folded = fromList (keep ++ updated)
   in folded

partial
solve : Input -> Nat
solve (pattern, (instruction :: _)) =
  size $ foldPaper instruction (fromList pattern)

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

solve2 : Input -> Paper
solve2 (pattern, instructions) =
  foldl (flip foldPaper) (fromList pattern) instructions

partial
main : IO ()
main = do Right input <- readFile "input13.txt"
          let Right parsed = parseInput input
            | Left err => putStrLn err
          putStrLn "Part 1:"
          printLn parsed
          printLn (solve parsed)
          putStrLn "Part 2:"
          putStrLn (printPaper (solve2 parsed))
          pure ()

