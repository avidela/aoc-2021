module Day2

import System.File
import Data.String
import Data.Either
import Debug.Trace
import Prolude
import Prolude.Parser

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

data Instruction = Up Integer | Down Integer | Forward Integer

parseInput1 : String -> Maybe (List Instruction)
parseInput1 x = traverse parseInstruction (lines x)
  where
    instr : Parser Instruction
    instr = Forward <$> (token "forward" *> integer)
        <|> Down <$> (token "down" *> integer)
        <|> Up <$> (token "up" *> integer)

    parseInstruction : String -> Maybe Instruction
    parseInstruction inp = map fst $ eitherToMaybe $ parse instr inp

solve1 : List Instruction -> Integer
solve1 instr = let finalState = foldr updateState (0,0) instr
                in fst finalState * snd finalState
  where
    updateState : Instruction -> (Integer, Integer) -> (Integer, Integer)
    updateState (Up k) (depth, pos) = (depth - k, pos)
    updateState (Down k) (depth, pos) = (depth + k, pos)
    updateState (Forward k) (depth, pos) = (depth, pos + k)

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

solve2 : List Instruction -> Integer
solve2 instr = let finalState = foldl (flip updateState) (0,0,0) instr
                   () = trace (show finalState) () in
                   fst finalState * fst (snd finalState)
  where
    updateState : Instruction -> (Integer, Integer, Integer) -> (Integer, Integer, Integer)
    updateState (Up k) (depth, pos, aim) = trace ("up by \{k}") (depth, pos, aim - k)
    updateState (Down k) (depth, pos, aim) = trace "down by \{k}" (depth, pos, aim + k)
    updateState (Forward k) st@(depth, pos, aim) = trace "forward by \{k}, with state \{st}" (depth + aim * k, pos + k, aim)

partial
main : IO ()
main = do Right input <- readFile "input2.txt"
          let Just parsed = parseInput1 input
          putStrLn "Part 1:"
          printLn (solve1 parsed)
          putStrLn "Part 2:"
          printLn (solve2 parsed)
          pure ()

