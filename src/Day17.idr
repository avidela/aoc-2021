module Day17

import Prolude
import Prolude.NGrid
import Debug.Trace

data InBounds = Before | Within | After


compareBounds : Ord a => a -> Bounds a -> InBounds
compareBounds x y = if x < y.lower
                       then Before
                       else if x > y.upper  then After
                       else Within

compareBounds' : Neg a => Num a => Ord a => a -> Bounds a -> InBounds
compareBounds' x bounds =
  if bounds.upper < 0
     then compareBounds (x - bounds.upper)
                        (MkBounds (bounds.lower - bounds.upper) 0)
     else compareBounds x bounds


Show i => Show (Bounds i) where
  show (MkBounds lower upper) = "[\{show lower} .. \{show upper}]"


---------------------------------------------------------------
Coordinate : Type
Coordinate = Vect 2 Int

Input : Type
Input = (Bounds Int, Bounds Int)

State : Type
State = (Coordinate, Coordinate)

updateVelocity : Coordinate -> Coordinate
updateVelocity [xv, xy] =
  let newY = xv - 1 in
      case compare xv 0 of
           EQ => [xv, newY]
           GT => [xv - 1, newY]
           LT => [xv + 1, newY]

updateState : State -> State
updateState (pos, vel) = (zipWith (+) pos vel, updateVelocity vel)

validXSpeeds : Input -> List Int
validXSpeeds (xBounds, _) = [0 .. xBounds.upper]

isValid : Bounds Int -> (pos, speed : Int) -> Bool
isValid range pos speed =
  case pos `compareBounds` range of
       Before => if speed == 0 then False else isValid range (pos + speed) (speed - 1)
       Within => True
       After => False

validYSpeeds : Bounds Int -> (time : Nat) -> (lastSpeed : Int) -> List Int
validYSpeeds verticalBounds time last =
  let v = traceMsg "obtained position using \{show last} as initial speed" $
          foldl (\pos, t => pos + last - t) (the Int 0) [ 0 .. (cast time - 1)] in
      case compareBounds' v verticalBounds of
           Within => traceMsg "\{show v} is within \{show verticalBounds}" $ v :: validYSpeeds verticalBounds time (last + 1)
           Before => traceMsg "\{show v} is after \{show verticalBounds}" $ []
           After => traceMsg "\{show v} is before \{show verticalBounds}" $ validYSpeeds verticalBounds time (last + 1)

computeTime : Input -> (speed, pos : Int) -> Nat -> Nat
computeTime b@(xbounds, _) speed pos time =
  if pos `inBounds` xbounds then time
                            else computeTime b (speed - 1) (pos + speed) (S time)

minimalXSpeed : Input -> Int
minimalXSpeed input = fromMaybe 0 $ head' (filter (isValid (fst input) 0) (validXSpeeds input))

solve : Input -> Nat
solve input@(xbounds, ybounds) =
  let xspeed = minimalXSpeed input
      timeAirborne = traceMsg "time airborne" $ computeTime input xspeed 0 0
      yspeeds =  validYSpeeds ybounds timeAirborne 1
   in trace (show yspeeds) 0



main : IO ()
main = do
  let input : Input = (MkBounds 20 30, MkBounds (-10) (-5))
  --printLn (validXSpeeds input)
  printLn (solve input)
  pure ()
