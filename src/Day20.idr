module Day20

import Prolude
import Prolude.NGrid
import Prolude.Cryptography
import Data.Nat
import Data.Fin
import Data.Nat.Views
import Data.Nat.Exponentiation
import Data.Monoid.Exponentiation

flatten : Vect n (Vect m a) -> Vect (n * m) a
flatten [] = []
flatten (x :: xs) = x ++ flatten xs

mapWithContext : (def : a) -> (Vect 3 (Vect 3 a) -> b) ->
                 NGrid m 2 a -> NGrid m 2 b
mapWithContext def f xs = ?mapWithContext_rhs

binaryToFin : {n : Nat} -> Vect n Bool -> Fin (2 `pow` n)
binaryToFin vect = let v = natToFin (binaryToDec vect) (2 `pow` n) in
                       fromMaybe ?impossibleCase v

getIndex : NGrid 3 2 Bool -> Vect 9 Bool
getIndex = flatten

[orSemi] Semigroup Bool where
  (<+>) a b= (||) a b

[orNeutral] Monoid Bool using orSemi where
  neutral = False

grow : (mon : Monoid a) =>  {x : Nat} -> NGrid x 2 a -> NGrid (2 + x) 2 a
grow grid = let e = emptyCube @{mon} x 1 in
                    (?fn grid)

Input : Nat -> Type
Input size = (Vect 512 Bool, NGrid size 2 Bool)

main : IO ()
main = putStrLn "Day 20"
