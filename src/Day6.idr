module Day6

import Data.Vect
import Data.List
import System.File
import Data.String
import Data.Either
import Data.String.Parser
import Data.SortedMap

import Debug.Trace

import Prolude
import Prolude.Parser

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

Population : Type
Population = Vect 9 Nat

parseInput1 : String -> Either String (List Nat)
parseInput1 = parseArray natural

toPopulation : List Nat -> Population
toPopulation xs = map (\i => count (==i) xs) (map (finToNat . fst) $ enumerate (replicate 9 0))

update : Population -> Population
update [d0,d1,d2,d3,d4,d5,d6,d7,d8] = [d1,d2,d3,d4,d5,d6,d7 + d0, d8, d0]

solve1 : List Nat -> Nat
solve1 initialState =
  let st = traceValue (toPopulation initialState)
      finalState = iterate 80 update st in
      sum finalState

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

solve2 : List Nat -> Nat
solve2 initialState =
  let st = traceValue (toPopulation initialState)
      finalState = iterate 256 update st in
      sum finalState

partial
main : IO ()
main = do Right input <- readFile "input6.txt"
          let Right (input) = parseInput1 input
            | Left err => putStrLn "couldn't parse \{err}"
          putStrLn "Part 1:"
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (solve2 input)
          pure ()
