module Day1

import Data.String
import Data.List
import Data.Vect
import Data.Nat
import System.File

import Prolude
import Prolude.Parser

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

zipList1Pair : List1 a -> List (a, a)
zipList1Pair xs = zip (forget xs) (tail xs)

solve : List1 Nat -> Nat
solve input = let pairs = zipList1Pair input in
                  count (uncurry (<)) pairs

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------


-- Sum each consecutive triple into a list
zipListTriples : Vect (2 + n) Nat -> Vect n Nat
zipListTriples input = zipWith3 (\a,b,c => a + b + c) (dropLastN 2 input) (dropLast (tail input)) (tail (tail input))

-- Parse the input as a vector of a least 3 elements
parseInput2 : String -> Maybe (n ** Vect (3 + n) Nat)
parseInput2 input = let Just (f :: s :: t :: xs) = traverse (the (String -> Maybe Nat) parsePositive) (lines input)
                          | _ => Nothing in
                        Just (_ ** (f :: s :: t :: fromList xs))

-- We sum the consecutive triples of the input vector
-- And then we count how many are smaller
solve2 : Vect (3 + n) Nat -> Nat
solve2 xs = let sums = zipListTriples xs in count (uncurry (<)) (zipVectConsecutive sums)

partial
main : IO ()
main = do Right input <- readFile "input1.txt"
          let Just parsed = parseList1 input
          putStrLn "Part 1:"
          printLn (solve parsed)
          putStrLn "Part 2:"
          let Just (_ ** parsed2) = parseInput2 input
          printLn (solve2 parsed2)
          pure ()

