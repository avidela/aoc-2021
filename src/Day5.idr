
module Day5

import Data.Vect
import Data.List
import System.File
import Data.String
import Data.Either
import Data.String.Parser
import Data.SortedMap

import Debug.Trace

import Prolude

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

-- Coordinates are pairs of positive numbers
Coordinate : Type
Coordinate = (Nat, Nat)

-- Lines are pairs of coordinate when we parse them
Line : Type
Line = (Coordinate, Coordinate)

-- After parsing we check what kind of line we have
-- Horizontal: Horizontal lines --
-- Vertial : Vertical lines |
-- DiaUp : Diagonal going up from left to right /
-- DiaDown : Diagonal going down from left to right \
data Axis = Horizonal | Vertical | DiaUp | DiaDown

-- Check if a line is either horizontal or vertical
isStraight : Axis -> Bool
isStraight Horizonal = True
isStraight Vertical = True
isStraight _ = False

Show Axis where
  show Horizonal = "-"
  show Vertical = "|"
  show DiaUp = "/"
  show DiaDown = "\\"

-- A straight line is defined by its axis, it's starting point and its length
-- for horizontal and diagonal lines, the starting point is the left-most point of the line
-- for vertical lines the starting point is the top-most point
StraightLine : Type
StraightLine = (Axis, Coordinate, Nat)

-- coordinates are numbers separated by a comma
parseCoord : Parser Coordinate
parseCoord = MkPair <$> natural <* char ',' <*> natural

-- a parser for lines parses a coordinate first, then an arrow, then another coordinate
parser : Parser Line
parser = do c1 <- lexeme parseCoord
            token "->"
            c2 <- lexeme parseCoord
            pure (c1, c2)

parseInput1 : String -> Either String (List Line)
parseInput1 input = traverse (map fst . parse parser) (lines input)

straightLine : Line -> StraightLine
straightLine inp@((x1,y1), (x2, y2)) =
  -- when the lines are vertical
  if x1 == x2
     then if y2 > y1 then traceMsg "\{show inp} output" $ (Vertical, (x1, y1), y2 `minus` y1)
                     else traceMsg "\{show inp} output" $ (Vertical, (x2, y2), y1 `minus` y2)
  -- when the lines are horizontal
  else if y1 == y2 then
     if x2 > x1 then traceMsg "\{show inp} output" $ (Horizonal, (x1, y1), x2 `minus` x1)
                else traceMsg "\{show inp} output" $ (Horizonal, (x2, y2), x1 `minus` x2)
  -- when the lines are diagonal
  else if x1 > x2
    then if y2 > y1 then traceMsg "\{show inp} output" $ (DiaUp, (x2, y2), x1 `minus` x2)
                    else traceMsg "\{show inp} output" $ (DiaDown, (x2, y2), x1 `minus` x2)
    else if y1 > y2 then traceMsg "\{show inp} output" $ (DiaUp, (x1, y1), x2 `minus` x1)
                    else traceMsg "\{show inp} output" $ (DiaDown, (x1, y1), x2 `minus` x1)

-- given a straight line, generate the list of coordinates it covers
-- This is done by taking the starting point and adding coordinates until our line is of
-- length 0. Because the starting point is always the left-most point, the `x` coordinate
-- is always increased
generateCoordinates : StraightLine -> List Coordinate
generateCoordinates (Horizonal, (c, Z)) = [c]
generateCoordinates (Horizonal, (c@(x, y), (S n))) = c :: generateCoordinates (Horizonal, (S x, y), n)
generateCoordinates (Vertical, (c, Z)) = [c]
generateCoordinates (Vertical, (c@(x, y), S n)) = c :: generateCoordinates (Vertical, (x, S y), n)
generateCoordinates (DiaDown, (c@(x,y), Z)) = [c]
generateCoordinates (DiaDown, (c@(x,y), S n)) = c :: generateCoordinates (DiaDown, (S x, S y), n)
generateCoordinates (DiaUp, (c@(x,y), Z)) = [c]
generateCoordinates (DiaUp, (c@(x,y), S n)) = c :: generateCoordinates (DiaUp, (S x, y `minus` 1), n)

-- To fill up the board we fill up a Map of coordinate as keys and number of overlaps as value, a missing
-- key is considered to be `0`. We then count the number of overlaps that are bigger or equal to 2
countFromLines : List StraightLine -> Nat
countFromLines ln = let filledBoard = foldl updateMap empty ln
                     in count (>= 2) (SortedMap.values filledBoard)
  where
    updateMap : SortedMap Coordinate Nat -> StraightLine -> SortedMap Coordinate Nat
    updateMap map line = foldl (\m, c => updateOrDefault c S 1 m) map (generateCoordinates line)

-- for the first part we need to remove the diagonal parts
solve1 : List Line -> Nat
solve1 xs = countFromLines (filter (isStraight . fst) $ map straightLine xs)

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

-- For the second part we use all lines, including diagonal ones
solve2 : List Line -> Nat
solve2 xs = countFromLines $ map straightLine xs

partial
main : IO ()
main = do Right input <- readFile "input5.txt"
          let Right (input) = parseInput1 input
            | Left err => putStrLn "couldn't parse \{err}"
          putStrLn "parsing done"
          putStrLn "Part 1:"
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (generateCoordinates (DiaDown, (1,1), 2))
          printLn (generateCoordinates (DiaUp, (7,9), 2))
          printLn (solve2 input)
          pure ()
