module Day14

import Prolude
import Prolude.SortedMap
import Prolude.Parser
import System.File

zipListConsecutive : (ls : List a) -> List (a, a)
zipListConsecutive [] = []
zipListConsecutive ls@(x :: xs) = zip ls xs

Interpolation Char where
  interpolate = show

Interpolation Nat where
  interpolate = show

Show k => Show v => Interpolation (SortedMap k v) where
  interpolate = show

groupBy : Eq a => List a -> List (a, Nat)

occurences : Eq a => Ord a => List a -> SortedMap a Nat
occurences ls = foldl (\map, key => updateOrDefault key S 1 map) (SortedMap.empty {k=a} {v=Nat}) ls

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

Operation : Type
Operation = ((Char, Char), Char)

Input : Type
Input = (List Char, List Operation)

parseInput1 : String -> Either String Input
parseInput1 = parse' (MkPair <$> many alphaNum <* spaces <*> (instructions `sepBy` spaces))
  where
    instructions : Parser (Operation)
    instructions = do c1 <- satisfy isUpper
                      c2 <- satisfy isUpper
                      spaces
                      token "->"
                      c3 <- satisfy isUpper
                      pure ((c1, c2), c3)
transform : List Operation -> (Char, Char) -> List Char
transform ops pair@(p1,p2) =
  case lookup pair ops of
       Nothing => [p2]
       Just v => [v, p2]

simulate : List Operation -> List Char -> List Char
simulate ops [] = []
simulate ops template@(z :: _) =
  let zipped =  zipListConsecutive template in
      z :: (concat $ map (transform ops) zipped)

partial
solve1 : Input -> Nat
solve1 (template, ops) =
  let v = iterate 10 (simulate ops) template
      grouped = map length $ (group (sort v))
      sorted@(s :: ss) = sort grouped
  in
      (last (s :: ss)) `minus` (head (s :: ss))

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

simulateFast : List Operation ->
               (SortedMap (Char, Char) Nat, SortedMap Char Nat) ->
               (SortedMap (Char, Char) Nat, SortedMap Char Nat)
simulateFast ops (pairs, counts) =
  let ls = foldl (update) (SortedMap.empty {k = (Char, Char)}, counts) $ SortedMap.toList pairs in
      ls
      where
        update : (SortedMap (Char, Char) Nat, SortedMap Char Nat) ->
                 ((Char, Char), Nat) -> (SortedMap (Char, Char) Nat, SortedMap Char Nat)
        update (mapping, counting) (pair@(p1, p2), count) =
          case lookup pair ops of
               Nothing => (updateOrDefault pair (+count) count mapping, counting)
               (Just insert) => (updateOrDefault (insert, p2) (+count) count
                               $ updateOrDefault (p1, insert) (+count) count
                               $ mapping
                               , updateOrDefault insert (+count) count counting)

extract : SortedMap (Char, Char) Nat -> List (Char, Nat)
extract = toList
        . foldl (\acc, ((p1, p2), v) => updateOrDefault p2 (+v) v
                                      $ updateOrDefault p1 (+v) v
                                      $ acc) (SortedMap.empty {k=Char} {v=Nat})
        . SortedMap.toList


adjust : List Char -> (SortedMap (Char, Char) Nat, SortedMap Char Nat)
adjust xs = (SortedMap.fromList . (map (,1)) . zipListConsecutive $ xs,
             occurences xs)

partial
countAnswer : Char -> Char -> SortedMap (Char, Char) Nat -> Nat
countAnswer c1 c2 mapping =
  let ls : List (Char, Nat) =
      concat $ map (\((k1, k2), count) => [(k2,count),(k2,count)])
                  (SortedMap.toList mapping)
      Just fstCount = S <$> lookup c1 ls
      Just lastCount = S <$> lookup c2 ls
      cleared = filter (\(k, v) => k /= c1 && k /= c2) ls
      updated = (fstCount) :: (lastCount) :: map snd cleared
      (s :: ss) = sort $ map (`div` 2) updated
  in last (s :: ss) `minus` s

partial
solve2 : Input -> Nat
solve2 (template@(t :: ts), ops) =
  let (mapping, v) = iterate 10 (simulateFast ops) (adjust template)
      (s :: ss) = traceMsg "sorted" $ sort $ values v
        | _ => 0
   in last (s :: ss) `minus` s
solve2 _ = Z

partial
main : IO ()
main = do Right input <- readFile "input14.txt"
          let Right (input) = parseInput1 input
            | Left err => putStrLn "couldn't parse \{err}"
          putStrLn "Part 1:"
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (solve2 input)
          pure ()
