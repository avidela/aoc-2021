module Day9

import Prolude

Board : Type
Board = Vect 5 (Vect 10 Nat)

Coordinate : Type
Coordinate = (Nat, Nat)

adjacent : Coordinate -> List Coordinate

parse : String -> Maybe Board

solve : Board -> Nat
solve board = sum . filter (\x => isSmallest board)
  where


main : IO ()
main = putStrLn "Day 9"
