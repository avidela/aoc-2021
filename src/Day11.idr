module Day11

import Prolude
import Prolude.NGrid
import Prolude.Parser
import Data.String
import Data.String.Parser
import Data.Nat
import Data.Fin
import Control.Monad.State
import Control.Monad.Identity

import System.File

import Debug.Trace

Board : Type
Board = NGrid 10 2 (Either Nat Nat)


SafeCoord : (d, n : Nat) -> Type
SafeCoord d n = Vect d (Fin n)

IBoard : Type
IBoard = NGrid 10 2 (SafeCoord 2 10, Either Nat Nat)

printBoard : IBoard -> String
printBoard vec = unlines $ map (concat . map (either show show . Builtin.snd) . toList) (toList vec)

addVect : Vect n Int -> Vect n Int -> Vect n Int
addVect = zipWith (+)

safeCast : Int -> Maybe Nat
safeCast n = if n < 0 then Nothing else Just (cast n)

zipWithIndex : Vect n a -> Vect n (Fin n, a)
zipWithIndex [] = []
zipWithIndex (x :: xs) = (FZ , x) :: map (mapFst FS) (zipWithIndex xs)

genCoordN : (n : Nat) -> List (Vect n Int)
genCoordN 0 = [[]]
genCoordN (S k) = let rec = genCoordN k
                      n1 = map (-1::) rec
                      n2 = map ( 0::) rec
                      n3 = map ( 1::) rec in n1 ++ n2 ++ n3

adjacentCorners : {s, n : Nat} ->  Coordinate n -> List (SafeCoord n s)
adjacentCorners coord =
  let directions : List (Vect n Int)
      directions =  filter (/= replicate n 0) (genCoordN n)

      intCoord : Vect n Int
      intCoord = map (cast {to=Int}) coord

      newDir : List (Vect n (Maybe Nat))
      newDir = map (map safeCast . addVect intCoord) directions

      natCoords : List (Vect n Nat)
      natCoords = catMaybes (map sequence newDir)

      finCoord : List (Vect n (Fin s))
      finCoord = catMaybes (map (traverse (\x => natToFin x s)) natCoords)
   in finCoord

total
mapBoard : (d : Nat) -> (f : a -> b) -> NGrid s d a -> NGrid s d b
mapBoard Z f x = f x
mapBoard (S k) f x = map (mapBoard k f) x

indexN' : {n : Nat} -> SafeCoord d n -> NGrid n d a -> a
indexN' [] x = x
indexN' (y :: xs) x =
  let v = (Vect.index y x) in
      indexN' xs v

flatten1 : Vect m (Vect n a) -> Vect (m * n) a
flatten1 [] = []
flatten1 (x :: xs) = x ++ flatten1 xs

flatten : {s, d : Nat} -> NGrid s d a -> Vect (s `power` d) a
flatten {s=s} {d=Z} x = [x]
flatten {s=0} {d=S m} x = []
flatten {s=(S k)} {d=S m} (x :: xs) =
  let rec = flatten x
      rec2 = map (flatten {d=m} {s=S k}) xs in
      rec ++ flatten1 {m=k} {n= power (S k) m} {a} rec2


-- overwrite the value at the given index
writeN : a -> {n, d : Nat} -> SafeCoord d n -> NGrid n d a -> NGrid n d a
writeN x [] y = x
writeN x (z :: xs) y =
  let v = writeN x xs (index z y) in Vect.replaceAt z v y

updateN : (a -> a) -> {n, d : Nat} -> SafeCoord d n -> NGrid n d a -> NGrid n d a
updateN f [] x = f x
updateN f (z :: xs) y =
  let v = updateN f xs (index z y) in Vect.replaceAt z v y

allN : (d : Nat) -> (a -> Bool) -> NGrid n d a -> Bool
allN Z f x = f x
allN (S n) f x = all (allN n f) x

traverseGrid : Applicative f => {s, d : Nat} -> (a -> f b) -> NGrid s d a -> f (NGrid s d b)
traverseGrid g x {d = 0}  = g x
traverseGrid g x {d = (S k)}  =
  traverse (traverseGrid g) x

generateIndices' : {s : Nat} -> (d : Nat) -> NGrid s d a -> (NGrid s d (SafeCoord d s, a))
generateIndices' 0 = ([],)
generateIndices' (S k) =
   map (\(i, b) => mkIndex k i (generateIndices' k b)) . zipWithIndex
  where
    mkIndex : (d : Nat) -> Fin s -> NGrid s d (SafeCoord k s, a)
                               -> NGrid s d (SafeCoord (S k) s, a)
    mkIndex d i = mapBoard d {a=(SafeCoord k s, a)} (\(vs, a) => (i :: vs, a))

iterateM : Monad m => Nat -> (a -> m a) -> a -> m a
iterateM 0 f x = f x
iterateM (S k) f x = f x >>= iterateM k f

----------------------------------------------------------------------

findIndex : IBoard -> Maybe (SafeCoord 2 10)
findIndex xs =
  let flat = flatten {s=10} {d=2} xs
      f = Vect.find ((> 9) . either id id . Builtin.snd) flat
   in map fst f

-- update the board until there are no more energy levels to update
-- first we find the next energy level to update
-- we then retrieve its adjacent cells
-- Then we update each adjacent cell of the board
-- Then we update the original cell to `Left 0` to indicate that it does
-- not recieve additional energy because it already flashed
updateIBoard : IBoard -> State Nat (Maybe IBoard)
updateIBoard bd = do let Just idx = findIndex bd
                       | Nothing => pure Nothing
                     let adjacents = adjacentCorners {s=10} (map finToNat idx)
                     let updated = foldr updateAtCoord bd adjacents
                     modify S
                     pure (Just (writeN (idx, Left 0) idx updated))
  where
    updateAtCoord : SafeCoord 2 10 -> IBoard -> IBoard
    updateAtCoord index = updateN (map (map S)) index

-- Perform a step of simulation
-- first we increment each energy level
-- then we loop throught he board until we are done propagating the energy levels
-- After we finished updating the board we reset the state of the board to allow each
-- cell to gain energy again
partial
updateNew : IBoard -> State Nat IBoard
updateNew xs = do let incremented = mapBoard 2 (map (map S)) xs
                  done <- loop incremented
                  pure $ mapBoard 2 (map (either Right Right)) done
  where
    loop : IBoard -> State Nat IBoard
    loop input = maybe (pure input) loop !(updateIBoard input)

parseInput1 : String -> Maybe Board
parseInput1 input = do v <- traverse (\str => traverse (map (Right {a=Nat} . cast {from = Integer} {to=Nat}) . parsePositive . cast) (unpack str)) (lines input)
                       traverse (toVect 10) v >>= toVect 10


-- Part 2

updateIBoard2 : IBoard -> (Maybe IBoard)
updateIBoard2 bd = do idx <- findIndex bd
                      let adjacents = adjacentCorners {s=10} (map finToNat idx)
                      let updated = foldr updateAtCoord bd adjacents
                      pure (writeN (idx, Left 0) idx updated)
  where
    updateAtCoord : SafeCoord 2 10 -> IBoard -> IBoard
    updateAtCoord xs = updateN (map (map S)) xs

updateBoard2 : IBoard -> IBoard
updateBoard2 xs = do let incremented = mapBoard 2 (map (map S)) xs
                     let done = loop incremented
                     mapBoard 2 (map (either Right Right)) done
  where
    loop : IBoard -> IBoard
    loop input = maybe (input) (loop) (updateIBoard2 input)

partial
iterateUntil : Nat -> (a -> Bool) -> (a -> a) -> a -> Nat
iterateUntil n f g x =
  if f x then n
         else iterateUntil (S n) f g (g x)

partial
iterateUntilM : Monad m => (a -> Bool) -> (a -> m a) -> a -> m a
iterateUntilM f g x = if (f x) then pure x else g x >>= iterateUntilM f g

partial
main : IO ()
main = do Right input <- readFile "input11.txt"
            | Left err => printLn err
          let Just parsed = map (generateIndices' 2) (parseInput1 input)
            | Nothing => putStrLn "parseError"
          putStrLn "Part 1:"
          printLn (execState Z (iterateM 99 updateNew parsed))
          putStrLn "Part 2:"
          printLn (allN 2 ((== 0) . either id id . snd) $ snd $ runState Z (iterateM 194 updateNew parsed))
          printLn ((iterateUntil 0 (allN 2 ((== 0) . either id id . snd)) updateBoard2 parsed))
          -- printLn (solve parsed)
