module Day3

import System.File

import Data.Vect
import Data.List
import Data.String
import Data.String.Parser
import Data.Either
import Debug.Trace
import Data.Nat

import Prolude
import Prolude.Cryptography

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------
parameters {width : Nat}

  parseInput1 : String -> Maybe (n ** Vect n (Vect width Bool))
  parseInput1 input = let lns = lines input
                          bools = traverse parseBoolean lns in
                          map (\ls => (_ ** fromList ls)) bools
    where
      parseNumber : Parser Bool
      parseNumber = char '0' *> pure False
                <|> char '1' *> pure True

      binary : (n : Nat) -> Parser (Vect n Bool)
      binary Z = pure []
      binary (S n) = do hd <- parseNumber
                        tl <- binary n
                        pure (hd :: tl)

      parseBoolean : String -> Maybe (Vect width Bool)
      parseBoolean x = map fst $ eitherToMaybe $ parse (binary width) x

  mostCommon : Vect n Bool -> Bool
  mostCommon xs = let (p1, p2) = partition id (toList xs)
                      numberOfTrues = length p1
                      numberOfFales = length p2
                  in numberOfTrues >= numberOfFales

  solve1 : (Vect n (Vect width Bool)) -> Nat
  solve1 xs = let gammaRate = map mostCommon (transpose xs)
                  epsilonRate = map not gammaRate
                  gammaDec = binaryToDec gammaRate
                  epsilonDec = binaryToDec epsilonRate in
                  gammaDec * epsilonDec

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------


  allIndicies : (n : Nat) -> Vect n (Fin n)
  allIndicies 0 = []
  allIndicies (S k) = FZ :: map FS (allIndicies k)

  filterDown : (compare : Bool -> Bool -> Bool) -> (i : Fin width) -> Vect n (Vect width Bool) -> (m ** Vect m (Vect width Bool))
  filterDown compare i input = let transposed = transpose input
                                   mostCommonBit = mostCommon (index i transposed) in
                                   filter (\vec => index i vec `compare` mostCommonBit) input

  solve2 : {n : Nat} -> (Vect n (Vect width Bool)) -> Maybe Nat
  solve2 xs = let en = allIndicies width
                  (1 ** [oxygen]) = iterate (\(_ ** acc), idx =>
                                    (filterDown (==) idx acc)) (_ ** xs) en
                    | _ => Nothing
                  (1 ** [scrubber]) = iterate (\(_ ** acc), idx =>
                                      (filterDown (/=) idx acc)) (_ ** xs) en
                    | _ => Nothing
               in Just $ binaryToDec oxygen * binaryToDec scrubber
    where
      iterate : (f : (n ** Vect n a) -> b -> (m ** Vect m a)) -> (acc : (n ** Vect n a)) -> Vect m b -> (o ** Vect o a)
      iterate f (MkDPair 1 vs) _ = (1 ** vs)
      iterate f acc [] = acc
      iterate f acc (x :: xs) = iterate f (f acc x) xs


partial
main : IO ()
main = do Right input <- readFile "input3.txt"
          let Just (_ ** parsed) = parseInput1 {width = 12} input
            | Nothing => putStrLn "couldn't parse"
          putStrLn "Part 1:"
          printLn (solve1 {width = 12} parsed)
          putStrLn "Part 2:"
          printLn (solve2 {width = 12} parsed)
          pure ()

