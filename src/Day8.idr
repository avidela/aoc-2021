module Day8

import System.File
import Data.Either
import Prolude
import Prolude.Parser
import Data.Vect
import Data.Vect.Extra
import Debug.Trace
--------------------------------------------------------------------------
-- Things that go in Prolude
--------------------------------------------------------------------------

insert : a -> Vect n a -> List (Vect (S n) a)
insert x [] = [[x]]
insert x (y :: xs) = (x :: y :: xs) :: (map (y ::) (insert x xs))

permutations : Vect n a -> List (Vect n a)
permutations []     = [[]]
permutations (x::xs) = concat [ insert x p | p <- permutations xs]


sort : Ord a => Vect n a -> Vect n a
sort xs = believe_me $ Vect.fromList (sort (toList xs))

contains : Eq a => Vect n a -> a -> Bool
contains [] x = False
contains (y :: xs) x = if x == y then True else contains xs x

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

data SegmentName = A | B | C | D | E | F | G

Show SegmentName where
  show A = "a"
  show B = "b"
  show C = "c"
  show D = "d"
  show E = "e"
  show F = "f"
  show G = "g"

Eq SegmentName where
  a == b = show a == show b

Ord SegmentName where
  compare a b = show a `compare` show b

record PartialAssignment where
  constructor MkAssign
  top : List SegmentName
  topLeft : List SegmentName
  topRight : List SegmentName
  middle : List SegmentName
  botLeft : List SegmentName
  botRight : List SegmentName
  bottom : List SegmentName

record ValidAssignement where
  constructor MkValid
  top : SegmentName
  topLeft : SegmentName
  topRight : SegmentName
  middle : SegmentName
  botLeft : SegmentName
  botRight : SegmentName
  bottom : SegmentName

data Signal
  =
    One (Vect 2 SegmentName)
  | Four (Vect 4 SegmentName)
  | Seven (Vect 3 SegmentName)
  | Eight (Vect 7 SegmentName)
  | Unknown5 (Vect 5 SegmentName)
  | Unknown6 (Vect 6 SegmentName)

displaySignal : Vect n SegmentName -> String
displaySignal xs = concat (map show xs)

Show Signal where
  show (One xs) = show $ map displaySignal (permutations xs)
  show (Four xs) = show $ map displaySignal (permutations xs)
  show (Seven xs) = show $ map displaySignal (permutations xs)
  show (Eight xs) = show $ map displaySignal (permutations xs)
  show (Unknown5 xs) = show $ map displaySignal (permutations xs)
  show (Unknown6 xs) = show $ map displaySignal (permutations xs)

InputLine : Type
InputLine = (Vect 10 Signal, Vect 4 Signal)

isUnique : Signal -> Bool
isUnique (Unknown5 _) = False
isUnique (Unknown6 _) = False
isUnique _ = True

parseSegmentName : Parser SegmentName
parseSegmentName = (char 'a' *> pure A)
               <|> (char 'b' *> pure B)
               <|> (char 'c' *> pure C)
               <|> (char 'd' *> pure D)
               <|> (char 'e' *> pure E)
               <|> (char 'f' *> pure F)
               <|> (char 'g' *> pure G)

parseSignal : Parser Signal
parseSignal = One      <$> ntimes 2 parseSegmentName <* spaces1
          <|> Four     <$> ntimes 4 parseSegmentName <* spaces1
          <|> Seven    <$> ntimes 3 parseSegmentName <* spaces1
          <|> Eight    <$> ntimes 7 parseSegmentName <* spaces1
          <|> Unknown5 <$> ntimes 5 parseSegmentName <* spaces1
          <|> Unknown6 <$> ntimes 6 parseSegmentName <* spaces1

parseInputLine : Parser InputLine
parseInputLine = MkPair <$> (ntimes 10 parseSignal) <*> (token "|" *> ntimes 4 parseSignal)

parseInput1 : String -> Either String (List (Vect 10 Signal, Vect 4 Signal))
parseInput1 input = parse' (parseInputLine `sepBy` spaces) input

solve1 : List InputLine -> Nat
solve1 signals = count isUnique $ signals >>= (toList . snd)

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

getNumber : Vect 4 Signal -> ValidAssignement -> Nat
getNumber [a,b,c,d] assign = number a * 1000 + number b * 100 + number c * 10 + number d
  where
    number : Signal -> Nat
    number (One xs) = 1
    number (Four xs) = 4
    number (Seven xs) = 7
    number (Eight xs) = 8
    number (Unknown5 xs) =
      if not (xs `contains` assign.botLeft)
         then if not (xs `contains` assign.topRight)
                 then 5 else 3
         else 2
    number (Unknown6 xs) =
      if xs `contains` (assign.middle)
         then if xs `contains` assign.botRight then 6 else 9
         else 0

getOne : Vect n Signal -> Maybe (Vect 2 SegmentName)
getOne [] = Nothing
getOne (One x :: xs) = Just x
getOne (_ :: xs) = getOne xs

getSeven : Vect n Signal -> Maybe (Vect 3 SegmentName)
getSeven [] = Nothing
getSeven (Seven x :: xs) = Just x
getSeven (_ :: xs) = getSeven xs

getFour : Vect n Signal -> Maybe (Vect 4 SegmentName)
getFour [] = Nothing
getFour (Four x :: xs) = Just x
getFour (_ :: xs) = getFour xs

getEight : Vect n Signal -> Maybe (Vect 7 SegmentName)
getEight [] = Nothing
getEight (Eight x :: xs) = Just x
getEight (_ :: xs) = getEight xs

difference : Eq a => {m : Nat} -> Vect n a -> Vect (n + m) a -> Maybe (Vect m a)
difference [] ys = Just ys
difference (x :: xs) (y :: ys) = if y == x
  then difference xs ys
  else case m of
            (S m') => let v : Maybe (Vect m' a) = difference (x :: xs) (believe_me ys) in
                          map (y ::) v
            Z => Nothing


Partial2 : Type
Partial2 = Vect 7 (List SegmentName)

validAssign : Partial2 -> Maybe ValidAssignement
validAssign [ [top]
            , [topLeft]
            , [topRight]
            , [middle]
            , [botLeft]
            , [botRight]
            , [bottom]
            ] = Just $ MkValid top topLeft topRight middle botLeft botRight bottom
validAssign _ = Nothing

partialAttempt : Fin 7 -> Partial2 -> Maybe ValidAssignement
partialAttempt FZ assign = validAssign assign
partialAttempt idx@(FS n) assign =
     if any id (map null assign)
            then Nothing
            else case index idx assign of
                   [] => Nothing
                   [_] => partialAttempt (weaken n) assign
                   (x :: xs) => let newAssign = replaceAt idx [x] $ map (filter (== x)) assign in
                                    case partialAttempt (weaken n) newAssign of
                                         Nothing => partialAttempt idx (replaceAt idx xs assign)
                                         Just x => Just x

generateAssignment : InputLine -> Nat
generateAssignment (mangled, number) = fromMaybe 0 (getNumber number <$> (deduceAssignement mangled))
  where
    deduceAssignement : Vect 10 Signal -> Maybe ValidAssignement
    deduceAssignement xs = do
      oneSeg@[mTopRight, mBotRight]<- sort <$> getOne xs
      sevSeg <- sort <$> getSeven xs
      fourSeg <- sort <$> getFour xs
      eightSeg <- sort <$> getEight xs
      [topSeg] <- difference oneSeg sevSeg
      -- [maybeTopLeft, maybeMiddle] <- traceMsg "foursegments" $ difference oneSeg fourSeg
      let candiates = [[topSeg], [A,B,C,D,E,F], [mTopRight, mBotRight]
                            , [A,B,C,D,E,F]
                      , [A,B,C,D,E,F], [mTopRight, mBotRight], [A,B,C,D,E,F]]
      partialAttempt 6 candiates

-- We sum the consecutive triples of the input vector
-- And then we count how many are smaller
solve2 : List InputLine -> Nat
solve2 xs = sum $ map generateAssignment xs

partial
main : IO ()
main = do Right input <- readFile "input8.txt"
          let Right parsed = parseInput1 input
            | Left err => putStrLn "parse error \{err}"
          putStrLn "Part 1:"
          printLn (solve1 parsed)
          putStrLn "Part 2:"
          printLn (solve2 parsed)
          pure ()
