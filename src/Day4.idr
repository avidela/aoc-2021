module Day4

import Data.Vect
import Data.List
import System.File
import Data.String
import Data.Either
import Data.String.Parser

import Debug.Trace

import Prolude

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

data Number = Marked Nat | Unmarked Nat

isMarked : Number -> Bool
isMarked (Marked _) = True
isMarked _ = False

markNumber : Nat -> Number -> Number
markNumber k (Marked j) = Marked j
markNumber k (Unmarked j) = if k == j then Marked k else Unmarked j

Show Number where
  show (Marked n) = show n
  show (Unmarked n) = show n

Eq Number where
  Marked n == Marked m = n == m
  Unmarked n == Unmarked m = n == m
  _ == _ = False

Board : Type
Board = Vect 5 (Vect 5 Number)

parseBoard : Parser Board
parseBoard = ntimes 5 (ntimes 5 (Unmarked <$> lexeme natural) <* spaces)

parser : Parser (List Nat, List Board)
parser = do numbers <- commaSep natural
            spaces
            boards <- sepBy parseBoard spaces
            pure (numbers, boards)

parseInput1 : String -> Either String ((List Nat, List Board))
parseInput1 input = map fst $ parse parser input

isWinner : Board -> Bool
isWinner board = any (all isMarked) board
              || any (all isMarked) (transpose board)

findWinner : List Board -> Maybe Board
findWinner = List.find isWinner

checkNumber : Nat -> Board -> Board
checkNumber n = map (map (markNumber n))
updateBoards : Nat -> List Board -> List Board
updateBoards n = map (checkNumber n)

runGame : List Nat -> List Board -> Maybe (Nat, List Nat, List Board)
runGame [] y = Nothing
runGame (x :: xs) boards = Just (x, xs, updateBoards x boards)

computeWin : Nat -> Board -> Nat
computeWin last board = let unmarkedSum = sum (map (foldr (\case (Marked _) => id
                                                                 (Unmarked n) => (n +)) Z) board)
                        in trace "computing score for board \{show board}\n sum \{unmarkedSum}\nwith product \{last * unmarkedSum}" $ last * unmarkedSum

solve1 : (List Nat, List Board) -> Maybe Nat
solve1 (numbers, boards) = do
  (last, rest, boards') <- runGame numbers boards
  maybe (solve1 (rest, boards')) (Just . computeWin last) (findWinner boards')

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

solve2 :  (List Nat, List Board) -> Maybe Nat
solve2 (numbers, boards) =
  let (vect@(_ :: _), _) = foldl (flip advance) ([], boards) numbers
        | _ => Nothing
   in trace (show vect) (Just $ List.head (vect))
  where
    advance : Nat -> (List Nat, List Board) -> (List Nat, List Board)
    advance number (winners, boards) =
      let newBoards = updateBoards number boards
          (newWinners, losers) = partition isWinner newBoards
       in (map (computeWin number) newWinners ++ toList winners, losers)

partial
main : IO ()
main = do Right input <- readFile "input4.txt"
          let Right (input) = parseInput1 input
            | Left err => putStrLn "couldn't parse \{err}"
          putStrLn "Part 1:"
          printLn input
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (solve2 input)
          pure ()
