module Day12

import Prolude
import Prolude.Parser
import System.File
import Debug.Trace
import Data.String

data Room = Start | End | Big String | Small String

Eq Room where
  Start == Start = True
  End == End = True
  (Big x) == (Big y) = x == y
  (Small x) == (Small y) = x == y
  _ == _ = False

Show Room where
  show Start = "start"
  show End = "end"
  show (Big n) = n
  show (Small n) = n

Interpolation Room where
  interpolate = show

upperString : Parser String
upperString = do c1 <- satisfy isUpper
                 c2 <- many alphaNum
                 pure (pack (c1 :: c2))

lowerString : Parser String
lowerString = do c1 <- satisfy isLower
                 c2 <- many alphaNum
                 pure (pack (c1 :: c2))

-------------------------------------
-- ^ goes into prolude
-------------------------------------

parse : String -> Either String (List (Vect 2 Room))
parse input = --  map (\ls => ls ++ map (reverse) ls) $
              map Builtin.fst $ parse (parseEdge `sepBy` spaces) input
  where
    parseRoom : Parser Room
    parseRoom = (token "start" *> pure Start)
            <|> (token "end" *> pure End)
            <|> Big <$> upperString
            <|> Small <$> lowerString

    parseEdge : Parser (Vect 2 Room)
    parseEdge = do v1 <- parseRoom <* token "-"
                   v2 <- parseRoom
                   pure [v1, v2]

getReachable : List (Vect 2 Room) -> Room -> List Room
getReachable [] x = []
getReachable ([r1,r2] :: xs) room =
  if r1 == room then r2 :: getReachable xs room
                else if r2 == room then r1 :: getReachable xs room
                else getReachable xs room


generatePaths : (List (Vect 2 Room)) -> List (List Room) -> (current : Room) -> List (List Room)
generatePaths graph acc End = map (End ::) acc
generatePaths graph acc current@(Big _)=
  let reachable = getReachable graph current in
      if null reachable then []
                        else
      let recPaths = map (\r => generatePaths graph (map (current ::) acc) r) reachable
       in concat (recPaths)
generatePaths graph acc current =
  let available = getReachable graph current in
      if null available then []
                        else
      let keep = List.filter (not . (elem current)) graph
          recPaths = map (\r => generatePaths keep (map (current ::) acc) r) available
       in concat (recPaths)

solve : List (Vect 2 Room) -> Nat
solve graph =
  let paths = generatePaths graph [[]] Start in
      length (paths)

----
-- part 2
-------------------

isSmall : Room -> Bool
isSmall (Small _) = True
isSmall _ = False

generatePaths' : (List (Vect 2 Room)) ->
                 Maybe Room ->
                 List Room ->
                 List (List Room) -> (current : Room) -> List (List Room)
generatePaths' graph small visited acc End = map (End :: ) acc
generatePaths' graph small visited acc current@(Big _)=
  let reachable = getReachable graph current in
      if null reachable then []
                        else
      let recPaths = map (\r => generatePaths' graph small visited (map (current ::) acc) r) reachable
       in concat (recPaths)
generatePaths' graph Nothing visited acc current@(Small n) =
    let available = getReachable graph current in
        if null available -- Nothing reachable, kill the branch
        then []
        else if current `elem` visited -- If we already tried visitn `current` twice
        then                           -- then don't pick it as a candidate for visiting twice
          let keep = List.filter (not . (elem current)) graph
              recPaths = map (\r => generatePaths' keep Nothing visited (map (current ::) acc) r) available
           in concat (recPaths)
        else                           -- Otherwise `current` is a candidate for visitng twice
                                       -- Thefore we do not remove from the list of edges
                                       -- and we add it to the list nodes we've attempted to
                                       -- visit twice
          let recPaths = map (\r => generatePaths'
                                        graph (Just current) (current :: visited)
                                        (map (current ::) acc) r)
                             available
           in concat (recPaths)
generatePaths' graph small@(Just visting) visited acc current@(Small v)=
  let available = getReachable graph current in
      if null available then []
                        else
      let keep = List.filter (not . (elem current)) graph
          recPaths = map (\r => generatePaths' keep small visited (map (current ::) acc) r) available
       in concat (recPaths)
generatePaths' graph small visited acc current =
  let available = getReachable graph current in
      if null available then []
                        else
      let keep = List.filter (not . (elem current)) graph
          recPaths = map (\r => generatePaths' keep small visited (map (current ::) acc) r) available
       in concat (recPaths)

solve2 : List (Vect 2 Room) -> List (List Room)
solve2 graph = generatePaths' graph Nothing [] [[]] Start

test : List (List Room)
test = [ [Start,Big "A",Small "b",Big "A",Small"b",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "b",Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "b",Big "A",Small "b",End]
, [Start,Big "A",Small "b",Big "A",Small "c",Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "b",Big "A",Small "c",Big "A",Small "b",End]
, [Start,Big "A",Small "b",Big "A",Small "c",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "b",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "b",Small "d",Small "b",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "b",Small "d",Small "b",Big "A",End]
, [Start,Big "A",Small "b",Small "d",Small "b",End]
, [Start,Big "A",Small "b",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Big "A",Small "b",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Small "d",Small "b",Big "A",End]
, [Start,Big "A",Small "c",Big "A",Small "b",Small "d",Small "b",End]
, [Start,Big "A",Small "c",Big "A",Small "b",End]
, [Start,Big "A",Small "c",Big "A",Small "c",Big "A",Small "b",Big "A",End]
, [Start,Big "A",Small "c",Big "A",Small "c",Big "A",Small "b",End]
, [Start,Big "A",Small "c",Big "A",Small "c",Big "A",End]
, [Start,Big "A",Small "c",Big "A",End]
, [Start,Big "A",End]
, [Start,Small "b",Big "A",Small "b",Big "A",Small "c",Big "A",End]
, [Start,Small "b",Big "A",Small "b",Big "A",End]
, [Start,Small "b",Big "A",Small "b",End]
, [Start,Small "b",Big "A",Small "c",Big "A",Small "b",Big "A",End]
, [Start,Small "b",Big "A",Small "c",Big "A",Small "b",End]
, [Start,Small "b",Big "A",Small "c",Big "A",Small "c",Big "A",End]
, [Start,Small "b",Big "A",Small "c",Big "A",End]
, [Start,Small "b",Big "A",End]
, [Start,Small "b",Small "d",Small "b",Big "A",Small "c",Big "A",End]
, [Start,Small "b",Small "d",Small "b",Big "A",End]
, [Start,Small "b",Small "d",Small "b",End]
, [Start,Small "b",End]]

main : IO ()
main = do Right file <- readFile "input12.txt"
            | Left err => printLn err
          let Right parsed = parse file
            | Left err => putStrLn err
          printLn parsed
          putStrLn "Part1:"
          printLn (solve parsed)
          putStrLn "Part2:"
          let paths = map reverse (solve2 parsed)
          let diff = filter (\path => not $ elem path paths) test
          putStrLn (unlines $ map show paths)
          putStrLn "diff:"
          putStrLn (unlines $ map show diff)
          pure()


