module Day7

import Data.Vect
import Data.List
import System.File
import Data.String
import Data.Either
import Data.String.Parser
import Data.SortedMap

import Debug.Trace
import Prolude
import Prolude.Parser

--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

-- Parse a list of integer
parseInput1 : String -> Either String (List Integer)
parseInput1 = parseArray integer

-- This maps the given list into a list of the absolute value of the difference with the value given
differences : (val : Integer) -> List Integer -> List Integer
differences val = map (\x => let diff = (val - x) in if diff < 0 then -1 * diff else diff)

-- To solve problem 1 we get the list of possible fuel consumptions
-- and we pick the smallest one
solve1 : List Integer -> Integer
solve1 positions = let mn = PositiveInteger.minimum positions
                       mx = PositiveInteger.maximum positions
                       differences = map (\m => sum $ differences m positions) [mn .. mx]
                       sorted = sort differences
                    in fromMaybe 0 (head' sorted)

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

fuelCost : (distance : Integer) -> Integer
fuelCost n = n * (n + 1) `div` 2

-- For problem 2 we add an adjusted fuel cost for each difference
solve2 : List Integer -> Integer
solve2 positions = let mn = PositiveInteger.minimum positions
                       mx = PositiveInteger.maximum positions
                       differences = map (\m => sum $ map fuelCost $ differences m positions) [mn .. mx]
                       sorted = sort differences
                    in fromMaybe 0 (head' sorted)

partial
main : IO ()
main = do Right input <- readFile "input7.txt"
          let Right (input) = parseInput1 input
            | Left err => putStrLn "couldn't parse \{err}"
          putStrLn "Part 1:"
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (solve2 input)
          pure ()
