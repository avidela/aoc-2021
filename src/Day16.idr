module Day16

import System.File
import Prolude
import Prolude.Cryptography
import Control.Monad.Identity
import Control.Monad.Trans
import Data.Either
import Data.String
import Debug.Trace

ByteStream : Type
ByteStream = List Bool

||| The input state, pos is position in the string and maxPos is the length of the input string.
public export
record State where
    constructor S
    input : ByteStream
    pos : Nat
    maxPos : Nat

Show State where
    show s = "(" ++ show s.input ++ ", " ++ show s.pos ++ ", " ++ show s.maxPos ++ ")"

||| Result of applying a parser
public export
data Result a = Fail Nat String | OK a State

Functor Result where
  map f (Fail i err) = Fail i err
  map f (OK r s)     = OK (f r) s

public export
record ParseT (m : Type -> Type) (a : Type) where
    constructor P
    runParser : State -> m (Result a)

public export
Parser : Type -> Type
Parser = ParseT Identity

public export
Functor m => Functor (ParseT m) where
    map f p = P $ \s => map (map f) (p.runParser s)

public export
Monad m => Applicative (ParseT m) where
    pure x = P $ \s => pure $ OK x s
    f <*> x = P $ \s => case !(f.runParser s) of
                            OK f' s' => map (map f') (x.runParser s')
                            Fail i err => pure $ Fail i err

public export
Monad m => Alternative (ParseT m) where
    empty = P $ \s => pure $ Fail s.pos "no alternative left"
    a <|> b = P $ \s => case !(a.runParser s) of
                            OK r s' => pure $ OK r s'
                            Fail _ _ => b.runParser s

public export
Monad m => Monad (ParseT m) where
    m >>= k = P $ \s => case !(m.runParser s) of
                             OK a s' => (k a).runParser s'
                             Fail i err => pure $ Fail i err

public export
MonadTrans ParseT where
    lift x = P $ \s => map (flip OK s) x

||| Run a parser in a monad
||| Returns a tuple of the result and final position on success.
||| Returns an error message on failure.
export
parseT : Functor m => ParseT m a -> List Bool -> m (Either String (a, Nat))
parseT p str = map (\case
                       OK r s => Right (r, s.pos)
                       Fail i err => Left $ "Parse failed at position \{show i}: \{err}")
                   (p.runParser (S str 0 (length str)))

||| Run a parser in a pure function
||| Returns a tuple of the result and final position on success.
||| Returns an error message on failure.
export
parse : Parser a -> ByteStream -> Either String (a, Nat)
parse p = runIdentity . parseT p

export
is : Applicative m => Bool -> ParseT m Bool
is b = P $ \s => pure $ case s.input of
    [] => Fail s.pos "Expected \{show b}, but input was empty"
    (x :: xs) => if x == b
                    then OK x (S xs (S s.pos) s.maxPos)
                    else Fail s.pos "Expected \{show b}, but got \{show x} instead"

next : Applicative m => ParseT m Bool
next = P $ \s => pure $ case s.input of
  [] => Fail s.pos "Expected input but it was empty"
  (x :: xs) => OK x (S xs (S s.pos) s.maxPos)

eat : Monad m => (n : Nat) -> ParseT m (Vect n Bool)
eat Z = pure []
eat (S n) = [| next :: eat n |]

match : Monad m => List Bool -> ParseT m (List Bool)
match [] = pure []
match (x :: xs) = [| is x :: match xs |]

binaryToNat : ByteStream -> Nat
binaryToNat ls = binaryToDec (fromList ls)

ntimes : (n : Nat) -> Parser a -> Parser (Vect n a)
ntimes 0 p = pure []
ntimes (S k) p = [| p :: ntimes k p |]


manyBounded : Monad m => (n : Nat) -> ParseT m a -> ParseT m (List a)
manyBounded n p = P $ \s =>
    do OK v s' <- p.runParser s
         | f => pure (map singleton f)
       let diff = s'.pos `minus` s.pos
       case compare diff n of
            EQ => pure (OK [v] s')
            GT => pure (Fail s'.pos "parse ate more than it was allowed")
            LT => do OK rest s'' <- runParser (manyBounded (n `minus` diff) p) s'
                       | f => pure f
                     pure (OK (v :: rest) s'')

-- parseT : Functor m => ParseT m a -> List Bool -> m (Either String (a, Nat))
-- parseT p str = map (\case
--                        OK r s => Right (r, s.pos)
--                        Fail i err => Left $ "Parse failed at position \{show i}: \{err}")
--                    (p.runParser (S str 0 (length str)))
--------------------------------------------------------------------------
-- Part 1
--------------------------------------------------------------------------

Input : Type
Input = ByteStream

fromInteger : Integer -> Bool
fromInteger 0 = False
fromInteger _ = True

parseInput1 : String -> Maybe Input
parseInput1 = map concat .  traverse charToHex . unpack
  where
  charToHex : Char -> Maybe (List Bool)
  charToHex '0' = Just [0,0,0,0]
  charToHex '1' = Just [0,0,0,1]
  charToHex '2' = Just [0,0,1,0]
  charToHex '3' = Just [0,0,1,1]
  charToHex '4' = Just [0,1,0,0]
  charToHex '5' = Just [0,1,0,1]
  charToHex '6' = Just [0,1,1,0]
  charToHex '7' = Just [0,1,1,1]
  charToHex '8' = Just [1,0,0,0]
  charToHex '9' = Just [1,0,0,1]
  charToHex 'A' = Just [1,0,1,0]
  charToHex 'B' = Just [1,0,1,1]
  charToHex 'C' = Just [1,1,0,0]
  charToHex 'D' = Just [1,1,0,1]
  charToHex 'E' = Just [1,1,1,0]
  charToHex 'F' = Just [1,1,1,1]
  charToHex _ = Nothing

data Packet : Type where
  Literal : (version : Vect 3 Bool) -> List (Vect 4 Bool) -> Packet
  Operator : (version : Vect 3 Bool) -> (id : Nat) -> (children : List Packet) -> Packet

Show Packet where
  show (Literal version xs) = show $ binaryToNat $ concat $ map toList xs
  show (Operator version id children) = "operator id: \{show id}\n"
                                    ++ unlines (map (assert_total show) children)

parseLiteral : Parser (List (Vect 4 Bool))
parseLiteral =
  case !next of
       True  => [| eat 4 :: parseLiteral |]
       False => (pure <$> eat 4)

mutual
  parseChildren : Parser (List Packet)
  parseChildren = case !next of
      False => do numberOfBits <- binaryToDec <$> eat 15
                  subSection <- manyBounded numberOfBits parsePacket
                  pure subSection
      True  => do numberOfPackets <- binaryToDec <$> eat 11
                  packets <- ntimes numberOfPackets parsePacket
                  pure (toList packets)

  parsePacket : Parser Packet
  parsePacket = do version <- eat 3
                   [1,0,0] <- eat 3
                     | id => Operator version (binaryToDec id) <$> parseChildren
                   value <- parseLiteral
                   pure (Literal version value)

parsePackets : Input -> Maybe Packet
parsePackets xs =
  map Builtin.fst $ eitherToMaybe $ parse parsePacket xs

sumVersion : Nat -> Packet -> Nat
sumVersion n (Literal xs val) = n + binaryToDec xs
sumVersion n (Operator xs ys zs) = foldl sumVersion (n + binaryToDec xs) zs

partial
solve1 : Input -> Maybe Nat
solve1 input = (sumVersion 0 <$> parsePackets input )

--------------------------------------------------------------------------
-- Part 2
--------------------------------------------------------------------------

interpret : Packet -> Maybe Nat
interpret (Literal version xs) =
  -- traceMsg "number" $
  Just $ binaryToNat $ concat $ map toList xs
interpret (Operator version 0 children) =
  -- traceMsg "sum" $
  map sum $ traverse interpret children
interpret (Operator version 1 children) =
  -- traceMsg "product" $
  map product $ traverse interpret children
interpret (Operator version 2 (c :: cs)) =
  do c' <- interpret c
     cs' <- traverse interpret cs
     pure $ foldl min c' (c' :: cs')
interpret (Operator version 3 (c :: cs)) =
  do c' <- interpret c
     cs' <- traverse interpret cs
     pure $ foldl max c' (c' :: cs')
interpret (Operator version 5 [c1, c2]) = do
  v1 <- interpret c1
  v2 <- interpret c2
  pure $ traceMsg "GT" $ if v1 > v2 then 1 else 0
interpret (Operator version 6 [c1, c2]) = do
  v1 <- interpret c1
  v2 <- interpret c2
  pure $ traceMsg "LT" $ if v1 < v2 then 1 else 0
interpret (Operator version 7 [c1, c2]) = do
  v1 <- interpret c1
  v2 <- interpret c2
  pure $ traceMsg "eq" $ if v1 == v2 then 1 else 0
interpret x = trace "could not handle \{show x}" $ Nothing


solve2 : Input -> Maybe Nat
solve2 input = let program = parsePackets input in
                   program >>= interpret

partial
main : IO ()
main = do Right input <- readFile "input14.txt"
          let Just (input) = parseInput1 "020D64AEE52E55B4C017938FBBAC2D6002A53D21F9E90C18023600B80021D0862DC1700043232C2284D3B0105007251DE33CF281802D0E7001A0958C3B6EB542D2014340010B89112E228803518E2047E0004322B4128352DFE72BFE1CC77000E226B92FF7F7F0F4899CCEB788FBA632A444019349E40A801CA941898B661ECBC40820061A78E254024C126797B31A804B27C0582B2D7D4AF02791E431531100B2458A6219D29CB6C4247F7D6DB27BCBA4065138014C05B00801CC0513280108047020106460079801000332200B60002832801C200718012801503801A800B02801723F9B90009D6600D44A87B0CC8010B89D0661F980331F20A44470076767F8EF75AA94F5E1E6E9790C9008BF801AB8002171CA2A45C100661FC508B911C8043EC00C224BB8A753A6677FDB7B8EA85932F4600BE0039138612F684AB86392889C4A201253C013100623D464834200CC1787D09E76FC78200A16603A543E6D9E695E4C74C012D004646D08CAF74391B4232BDD1E4FFEE033805B3DAB074ACF351399FCCEA5F592697E1CB802B2D1D0BCFE410C015B004E46BE17973C949C213153005A6932C0129BDF675DD2CBF3482401BE7802D37AA4DFE6F549BD4A42363A200D5F40149985FEDF2ACF35AB4BD3003004A730F74019B8803F08A0943B1007A21C2487C0002DC578BC600A497B35A8050020F24432444401415002AF07A7F7FE004DB93001A931FC33A802B37FB517A4A52254010E2374C637895BF7E5CC66F53EB0CC2F4C92080292B1E7A0DB26BE6008CE1ACC801804938F530A1227F2A6A4004349A31009F7801A900021908A18C5D100722C43C8F9312CFD4040269934949661E0096FE75092ACA4F0B6A005CD6CBE1218027258AA3F00439377F5D566E338D121C0239DD9C4942FA4E8F73DFA62656402704E523896FAE9E00B4E779DE6BF15595C56DBF0ACD391802F400FA4FEADD769FD5BAE7318FCF32AB8"
            | Nothing => putStrLn "couldn't parse"
          putStrLn "Part 1:"
          printLn (solve1 input)
          putStrLn "Part 2:"
          printLn (solve2 input)
          pure ()
